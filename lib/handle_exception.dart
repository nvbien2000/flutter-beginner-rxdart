import 'dart:ffi';

Stream<int> getNumbersError() async* {
  for (var i = 1; i <= 3; i++) {
    yield i;
    await Future.delayed(Duration(seconds: 2));
    if (i == 2) throw Exception();
  }
}

Stream<int> getNumbersTimeout() async* {
  for (var i = 1; i <= 3; i++) {
    yield i;
    await Future.delayed(Duration(seconds: 3));
  }
}

handleError() => getNumbersError()
                  .handleError((e) => print(e)) //Show popup...
                  .listen(print);

timeout() => getNumbersTimeout()
              .timeout(Duration(seconds: 2)) //Show popup...
              .listen(print);

void main4() async {
  // handleError();
  timeout();
}