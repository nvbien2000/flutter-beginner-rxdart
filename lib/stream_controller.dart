import 'dart:async';

/*
 * What are differences between Stream & StreamController?
 * Nếu các sự kiện trong Stream đến từ các phần khác nhau trong chương trình
 *    chứ không chỉ đến từ 1 Stream hay Future (trong class getNumbers()) như hàm add()
 * StreamController giúp kiểm soát Stream từ bất cứ đầu (trong hàm add(), trong main())
 */
var _controller = StreamController<String>(); //add <>.broadcast()
Stream<String> get out => _controller.stream; //private

main5() {
  out.listen((data) => print(data));

  // only can listen 1 time
  // out.listen((data) => print(data.replaceAll('a', 'b')));
  add();
}

add() {
  _controller.sink.add('some data');
}