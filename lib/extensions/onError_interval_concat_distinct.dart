import 'package:rxdart/rxdart.dart';

import '../classes/merge_stream.dart';

Stream<int> errorStream() async* {
  yield 1;
  yield 2;
  throw FormatException('lỗi rồi');
  yield 3;
}

void mainz() {
  // ? Stream.froIterable() called recovery stream 
  // ? -> values from that stream was emitted instead of error, no print error
  errorStream().onErrorResumeNext(Stream.fromIterable([11, 22]))
    .listen(print, onError: print);

  //? The same with TimerStream, after 1s -> emit value
  // Stream.fromIterable([1, 2, 3])
  //   .interval(Duration(seconds: 1))
  //   .listen((i) => print('$i second'));
 
  //? concat
  // var currentTime = DateTime.now();
  // var concatStream = firstStream().concatWith([secondStream()]);
  // concatStream.listen((event) => myPrint(event, currentTime));

  //? distinct ONLY the previous event
  // Stream.fromIterable([1, 2, 2, 1, 1, 2, 3]).distinct()
  //     .listen(print); // print: 1, 2, 1, 2, 3

  //? distinct ALL events from source stream
  // Stream.fromIterable([1, 2, 2, 1, 2, 3]).distinctUnique()
  //     .listen(print); // print: 1, 2, 3
}