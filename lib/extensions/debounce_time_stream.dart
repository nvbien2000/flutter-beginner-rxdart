import 'package:rxdart/rxdart.dart';

void mainx() {
   // 500ms sau mà user không gõ nữa thì sẽ call API search
  demoStream().debounceTime(Duration(seconds: 1)).listen((event) { 
    print('call API search với keyword: $event');
  });
}

// Hàm này cực kỳ hữu ích để làm tính năng live search cho app (real time search) 
// bởi vì app sẽ tránh spam server do call API quá nhiều
// stream mô phỏng lúc user đang gõ text vào TextField để search
Stream<String> demoStream() async* {
  yield 'L'; // gỡ chữ L
  yield 'Le'; // gõ thêm chữ e
  yield 'Lea'; // gõ thêm chữ a
  yield 'Lear'; // gõ thêm chữ r
  yield 'Learn'; // gõ thêm chữ n
  await Future.delayed(Duration(seconds: 1)); // dừng gõ 1 giây
  yield 'Learn R'; // tiếp tục gõ dấu space và chữ R
  yield 'Learn Rx'; // gõ tiếp chữ x
  yield 'Learn RxD'; // gõ tiếp chữ D
  yield 'Learn RxDa'; // gõ tiếp chữ a
  yield 'Learn RxDar'; // gõ tiếp chữ r
  yield 'Learn RxDart'; // gõ tiếp chữ t
  await Future.delayed(Duration(seconds: 2));
}