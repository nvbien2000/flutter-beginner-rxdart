import 'package:rxdart/rxdart.dart';

void mainy() {
  //! Stream.froIterable() called recovery stream 
  //! -> values from that stream was emitted instead of error, no print error
  // errorStream().onErrorResumeNext(Stream.fromIterable([11, 22]))
  //   .listen(print, onError: print);

  Stream.fromIterable([1, 2, 3])
    .interval(Duration(seconds: 1)) // cứ 1 giây emit ra 1 event
    .listen((i) => print('$i giây')); // prints 1 giây, 2 giây, 3 giây
}

Stream<int> errorStream() async* {
  yield 1;
  yield 2;
  throw FormatException('lỗi rồi');
  yield 3;
}