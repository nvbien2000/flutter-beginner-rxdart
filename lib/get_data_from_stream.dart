Stream<int> getNumbersException() async* {
  for (var i = 1; i <= 3; i++) {
    yield i;
    await Future.delayed(Duration(seconds: 1));
    if (i == 2) throw Exception(); 
  }
}

Stream<int> getNumbers() async* {
  for (var i = 1; i <= 3; i++) {
    yield i;
    await Future.delayed(Duration(seconds: 1));
  }
}

listen() {
  // getNumbers().listen((data) {
  //   print(data);
  // });

  getNumbersException()
    .listen((data) => print(data))
    .onError((error) => print("THERE IS AN ERROR!"));

  // var sum = 0;
  // getNumbers()
  //   .listen((data) {
  //     sum += data;
  //   })
  //   .onDone(() => print(sum));
}

awaitFor() async {
  /* Wait until all values are emitted in stream
   * Like for loop but useful when we want to iterate until
   *    the data inside stream is end
   */
  var sum = 0;
  await for (var number in getNumbers()) {
    sum += number;
  }
  print(sum);
}

isEmpty() async {
  /**
   * Let us know whether stream is empty/closed or not
   * If run this function, it's stop immediately because at the 
   *    beginning, stream is already empty
   * Comment yield --> 3 seconds later
   */
  if (await getNumbers().isEmpty) {
    print('Stream is empty');
  } else {
    print('Stream is not empty');
  }
}

// Comment yield --> Error
firstNumber() async => print(await getNumbers().first);
lastNumber() async => print(await getNumbers().last);
getLength() async => print(await getNumbers().length);

/* Allow to emit only 1 value (if more, throw exception) */
single() async => print(await getNumbers().single);

any() async {
  /* Check if some kind of value meets some conditions 
   * Change to i < 2 --> Faster because check for every value emitted
   */
  if (await getNumbers().any((int i) => i < 2)) {
    print("There is a number equal to 3");
  } else {
    print("There isn\'t a number equal to 3");
  }
}

contain() async {
  /* Change to contain(1) --> Faster because check for every value emitted*/
  if (await getNumbers().contains(1)) {
    print("There is a number equal to 3");
  } else {
    print("There isn\'t a number equal to 3");
  }
}

/* Change to elementAt(0), elementAt(10)*/
elementAt() async => print(await getNumbers().elementAt(2));

/* Change to firstWhere(10)*/
firstWhere() async => print(await getNumbers().firstWhere((i) => i > 1));
lastWhere() async => print(await getNumbers().lastWhere((i) => i > 1));

/* Put everything into a string*/
join() async => print(await getNumbers().join(', '));

/* Check if there ONLY ONE value meets the condition*/
singleWhere() async => print(await getNumbers().singleWhere((i) => i > 1));


void main2() async {
  // listen();
  // awaitFor();
  // isEmpty();
  // firstNumber();
  // lastNumber();
  // getLength();
  // single();
  // any();
  // contain();
  // elementAt();
  // firstWhere();
  join();
}
