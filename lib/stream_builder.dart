import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

main6() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Home3());
  }
}

Stream<int> getNumbers() async* {
  for (var i = 1; i <= 3; i++) {
    yield i;
    await Future.delayed(Duration(seconds: 2));
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int? currentNumber;

  @override
  void initState() {
    getNumbers().listen((data) => setState(() => currentNumber = data));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("StreamBuilder")),
      body: Center(
          child: Text(
        currentNumber == null ? "No data" : currentNumber.toString(),
        style: TextStyle(fontSize: 50),
      )),
    );
  }
}

class Home2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("StreamBuilder")),
      body: Center(
          child: StreamBuilder<int>(
              stream: getNumbers().map((data) => data * 10),
              builder: (context, snapshot) {
                return Text(
                  snapshot.hasData ? "${snapshot.data}" : "No data",
                  style: TextStyle(fontSize: 50),
                );
              })),
    );
  }
}

class HomeModel {
  final _numbersController = StreamController<int>.broadcast();
  Stream<int> get outNumbers => _numbersController.stream;
  // The most common methods of Sink is add(data), addError() & close()
  Sink<int> get inNumbers => _numbersController.sink;

  // close old StreamController & prevent memory leaks
  void dispose() => _numbersController.close();
}

class Home3 extends StatelessWidget {
  final model = HomeModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("StreamBuilder")),
      body: Center(
          child: Column(
        children: [
          // 2 class communicate not directly but within Stream
          Widget1(model),
          Widget2(model),
        ],
      )),
    );
  }
}

class Widget1 extends StatelessWidget {
  Widget1(this._model);
  final HomeModel _model;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
      stream: _model.outNumbers,
      builder: (context, snapshot) {
        return Text(
          snapshot.hasData ? "${snapshot.data}" : "No data",
          style: TextStyle(fontSize: 50),
        );
      },
    );
  }
}

class Widget2 extends StatelessWidget {
  Widget2(this._model);
  final HomeModel _model;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () => _model.inNumbers.add(Random().nextInt(200)),
      child: Text("BUTTON"));
  }
}
