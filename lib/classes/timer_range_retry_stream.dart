import 'dart:math';

import 'package:english_words/english_words.dart';
import 'package:rxdart/rxdart.dart';

void mainC() {
  Rx.timer('hi', Duration(seconds: 3)) //Rx.timer(), sau 3s mới được emit
      .listen((i) => print(i));

  //* Returns a Stream that emits a sequence of Integers within a specified range
  // RangeStream(1, 6).listen((i) => print(i));

  /*
   * If stream has error, RetryStream allow stream to retry it by
   *    count number (2 in this case) and infinity if count == null
   * If stream has not terminated successfully, all of the errors and StackTraces
   *    that caused the failure will be emitted.
   */
  // RetryStream(() { return errorStream(); }, 3)
  //     .listen(print, onError: print);

  /// For more classes: [https://pub.dev/packages/rxdart#stream-classes]
}
  


Stream<int> errorStream() async* {
  yield Random().nextInt(100);
  throw FormatException(WordPair.random().asCamelCase);  // lỗi ở đây
  yield 11; // 11 ko bao h đc in ra
}