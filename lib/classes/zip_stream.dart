import 'package:rxdart/rxdart.dart';

void mainB() {
  // biến này để làm cột mốc thời gian lúc bắt đầu run chương trình
  var currentTime = DateTime.now();

  // var zipStream = Rx.zip2(firstStream(), secondStream(), (String a, String b) => a+b);
  var zipStream = ZipStream([firstStream(), secondStream()], (values) => values.join());
  zipStream.listen((event) => myPrint(event, currentTime));
}

// stream 1
Stream<String> firstStream() async* {
  await Future.delayed(Duration(seconds: 1));
  yield '1';
  await Future.delayed(Duration(seconds: 2));
  yield '2';
  await Future.delayed(Duration(seconds: 5));
  yield '3'; 
  await Future.delayed(Duration(seconds: 1));
  yield '4';
  await Future.delayed(Duration(seconds: 2));
  yield '5'; // emit 100 vào giây thứ 13
}

// stream 2
Stream<String> secondStream() async* {
  await Future.delayed(Duration(seconds: 2)); // sau 7s sẽ emit event đầu tiên
  yield 'A';
  await Future.delayed(Duration(seconds: 2));
  yield 'B';
  await Future.delayed(Duration(seconds: 2));
  yield 'C';
  await Future.delayed(Duration(seconds: 1));
  yield 'D';
}

// mình sử dụng hàm println này để in kèm thời điểm emit event cho dễ quan sát output
void myPrint(Object value, DateTime currentTime) {
  print('Emit $value vào giây thứ ${DateTime.now().difference(currentTime).inSeconds}');
}