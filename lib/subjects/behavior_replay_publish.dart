import 'package:rxdart/rxdart.dart';

var streamController = BehaviorSubject(); //* Take the latest data
// var streamController = BehaviorSubject.seeded(333); //* constructor
// var streamController = ReplaySubject(); //* Take all data added into stream
// * The same as StreamController.broadcast(), take no previous data
// var streamController = PublishSubject(); 

late DateTime currentTime;

void mainQ() {
  currentTime = DateTime.now();

  //Todo: first subscription
  streamController.stream.listen((event) => myPrint('thứ 1: $event'));

  //Todo: second subscription after 2s
  createSecondSubscription();

  // cứ mỗi 600 ms sẽ push một data từ 0 cho đến 4
  streamController.sink.addStream(RangeStream(0, 4).interval(Duration(milliseconds: 600)));
}

void createSecondSubscription() async {
  Future.delayed(Duration(seconds: 2), () {
    print('\n');
    myPrint('tạo ra subscription thứ 2');
    print('\n');

    streamController.stream.listen((event) {
      myPrint('subscription thứ 2: ${event}xxx');
    });
  });
}

void myPrint(Object value) {
  print('${DateTime.now().difference(currentTime).inMilliseconds}: $value');
}