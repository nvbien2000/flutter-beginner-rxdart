import 'dart:math';

import 'package:rxdart/rxdart.dart';

void main() async {
  // giả sử chúng ta có 3 subscription
  var subscription1 = testStream().listen(print);
  var subscription2 = testStream().listen(print);
  var subscription3 = testStream().listen(print);

  // tạo ra 1 compositeSubscription
  var compositeSubscription = CompositeSubscription();

  // add 3 subscription đó vào compositeSubscription
  compositeSubscription.add(subscription1);
  compositeSubscription.add(subscription2);
  compositeSubscription.add(subscription3);

  await Future.delayed(Duration(seconds: 2));
  compositeSubscription.clear();

  // if use clear(), we can reuse stream - opposed to dispose()
  // compositeSubscription.dispose();
}

Stream<int> testStream() async* {
  yield Random().nextInt(15);
  yield Random().nextInt(15);
  await Future.delayed(Duration(seconds: 4));
  yield Random().nextInt(1000);
}