import 'dart:math';

Stream<int> getNumbersDuplicates() async* {
  for (var i = 1; i <= 3; i++) {
    yield i;
    yield i;
    await Future.delayed(Duration(seconds: 1));
  }
}

Stream<int> getNumbers() async* {
  for (var i = 1; i <= 3; i++) {
    yield i;
    await Future.delayed(Duration(seconds: 1));
  }
  // yield 1;
  // yield 2;
  // yield 3;
  // yield 11;
  // yield 1;
}


/*
 * Transforms each element of this stream into a sequence of elements.
 * Returns a new stream where each element of this stream is replaced
 */
expand() async => getNumbers()
                    .expand((data) => [123, data*10]) //iterable field
                    .listen((item) => print(item));
/*
 * map() change the old value to new value
 * expand() change the old value to 1 or more values
 */
map() async {
  // getNumbers()
  //   .map((data) => [123, data*10])
  //   .listen((item) => print(item));

  getNumbers()
    .map((i) => i%2==0 ? i*2 : i-1)
    .listen((item) => print(item));
}

// Skip 2 values from stream (skip(5) didn't cause an error)
skip() => getNumbers().skip(2).listen((item) => print(item));

/* Skip the values meet condition
 * If 1 value doesn't meet condition (change to i < 11), all value
 *    after that will be emitted no matter condition true or false
 */
skipWhile() => getNumbers()
                .skipWhile((i) => i < 3)
                .listen((item) => print(item));

/* Opposite skip -> take X elements 
 */
take() => getNumbers().take(2)
              .listen((item) => print(item));
/* Only print 1 becuz 2 is divided by 2 (condition is broken), 
 *    so nothing was emitted after that
 */
takeWhile() => getNumbers().takeWhile((i) => i%2!=0)
              .listen((item) => print(item));
// where() filter everything, not like takeWhile()
where() => getNumbers().where((i) => i%2!=0)
              .listen((item) => print(item));             
// Skips data events if they are equal to the previous data event
distinct() => getNumbersDuplicates().distinct()
                .listen((item) => print(item));  
// Combine many actions
chaining() => getNumbersDuplicates()
                .distinct() // 1 2 3
                .map((item) => item*10) // 10 20 30
                .where((item) => item != 20) // 10 30
                .listen((item) => print(item)); // 10 30

void main3() async {
  // expand();
  // map(); 
  // skip();
  // skipWhile();
  // take();
  // takeWhile();
  // where();
  // distinct();
  chaining();
}
