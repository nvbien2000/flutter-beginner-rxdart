/*
 * 1. emit stream: async* & yield
 * 2. listen stream: listen whenever value is emitted
 */

Stream<int> getNumbers() async* {
  for (var i = 0; i < 10; i++) {
    yield i;
    await Future.delayed(Duration(seconds: 1));
  }
}

void main1() async {
  getNumbers().listen((data) {
    print(data);
  });
}
